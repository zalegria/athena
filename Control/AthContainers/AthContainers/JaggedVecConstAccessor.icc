/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration.
 */
/**
 * @file AthContainers/JaggedVecConstAccessor.icc
 * @author scott snyder <snyder@bnl.gov>
 * @date Apr, 2024
 * @brief Helper class to provide constant type-safe access to aux data.
 */


#include "AthContainers/AuxElement.h"
#include "AthContainers/AuxTypeRegistry.h"
#include "AthContainers/exceptions.h"


namespace SG {


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 *
 * The name -> auxid lookup is done here.
 */
template <class PAYLOAD_T, class ALLOC>
inline
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::ConstAccessor (const std::string& name)
  : ConstAccessor (name, "", AuxVarFlags::None)
{
}


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 * @param clsname The name of its associated class.  May be blank.
 *
 * The name -> auxid lookup is done here.
 */
template <class PAYLOAD_T, class ALLOC>
inline
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::ConstAccessor
  (const std::string& name, const std::string& clsname)
  : ConstAccessor (name, clsname, AuxVarFlags::None)
{
  // cppcheck-suppress missingReturn; false positive
}


/**
 * @brief Constructor taking an auxid directly.
 * @param auxid ID for this auxiliary variable.
 *
 * Will throw @c SG::ExcAuxTypeMismatch if the types don't match.
 */
template <class PAYLOAD_T, class ALLOC>
inline
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::ConstAccessor (const SG::auxid_t auxid)
{
  AuxTypeRegistry& r = AuxTypeRegistry::instance();
  this->m_auxid = auxid;
  r.checkAuxID<Elt_t, ALLOC> (this->m_auxid);
  this->m_linkedAuxid = r.linkedVariable (this->m_auxid);
  if (this->m_linkedAuxid == static_cast<uint32_t>(null_auxid)) {
    throw SG::ExcNoLinkedVar (auxid, typeid (Payload_t));
  }
}


/**
 * @brief Constructor.
 * @param name Name of this aux variable.
 * @param clsname The name of its associated class.  May be blank.
 * @param flags Optional flags qualifying the type.  See AuxTypeRegsitry.
 *
 * The name -> auxid lookup is done here.
 */
template <class PAYLOAD_T, class ALLOC>
inline
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::ConstAccessor
  (const std::string& name,
   const std::string& clsname,
   const AuxVarFlags flags)
{
  AuxTypeRegistry& r = AuxTypeRegistry::instance();
  m_linkedAuxid = r.getAuxID<Payload_t, PayloadAlloc_t> (AuxTypeRegistry::linkedName (name),
                                                         clsname,
                                                         flags | AuxVarFlags::Linked);
  m_auxid = r.getAuxID<Elt_t, ALLOC> (name, clsname, flags, m_linkedAuxid);
}


/**
 * @brief Constructor.
 * @param b Another accessor from which to copy the auxids.
 */
template <class PAYLOAD_T, class ALLOC>
inline
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::ConstAccessor (const detail::LinkedVarAccessorBase& b)
  : detail::LinkedVarAccessorBase (b)
{
  // cppcheck-suppress missingReturn; false positive
}


/**
 * @brief Fetch the variable for one element, as a const reference.
 * @param e The element for which to fetch the variable.
 */
template <class PAYLOAD_T, class ALLOC>
template <IsConstAuxElement ELT>
inline
auto
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::operator() (const ELT& e) const -> const element_type
{
  const AuxVectorData* container = e.container();
  assert (container != nullptr);
  return (*this)(*container, e.index());
}


/**
 * @brief Fetch the variable for one element, as a const reference.
 * @param container The container from which to fetch the variable.
 * @param index The index of the desired element.
 *
 * This allows retrieving aux data by container / index.
 */
template <class PAYLOAD_T, class ALLOC>
inline
auto
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::operator()
  (const AuxVectorData& container,  size_t index) const -> const element_type
{
  const Elt_t& jelt = container.template getData<Elt_t> (m_auxid, index);
  if (jelt.end() == 0) return element_type();
  const Payload_t* payload = this->getPayloadArray (container);
  return element_type (payload+jelt.begin (index), payload+jelt.end());
}


/**
 * @brief Get a pointer to the start of the array of @c JaggedVecElt objects.
 * @param container The container from which to fetch the variable.
 */
template <class PAYLOAD_T, class ALLOC>
inline
auto
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::getEltArray (const AuxVectorData& container) const
  -> const Elt_t*
{
  return reinterpret_cast<const Elt_t*> (container.getDataArray (m_auxid));
}


/**
 * @brief Get a pointer to the start of the payload array.
 * @param container The container from which to fetch the variable.
 */
template <class PAYLOAD_T, class ALLOC>
inline
auto
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::getPayloadArray (const AuxVectorData& container) const
  -> const Payload_t*
{
  return reinterpret_cast<const Payload_t*>
    (container.getDataArray (m_linkedAuxid));
}


/**
 * @brief Get a span over the array of @c JaggedVecElt objects.
 * @param container The container from which to fetch the variable.
 */
template <class PAYLOAD_T, class ALLOC>
inline
auto
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::getEltSpan (const AuxVectorData& container) const
  -> const_Elt_span
{
  auto beg = reinterpret_cast<const Elt_t*> (container.getDataArray (m_auxid));
  return const_Elt_span (beg, container.size_v());
}


/**
 * @brief Get a span over the payload vector.
 * @param container The container from which to fetch the variable.
 */
template <class PAYLOAD_T, class ALLOC>
inline
auto
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::getPayloadSpan (const AuxVectorData& container) const
  -> const_Payload_span
{
  const SG::AuxDataSpanBase* sp = container.getDataSpan (m_linkedAuxid);
  return const_Payload_span (reinterpret_cast<const Payload_t*>(sp->beg),
                             sp->size);
}


/**
 * @brief Get a span over spans representing the jagged vector.
 * @param container The container from which to fetch the variable.
 */
template <class PAYLOAD_T, class ALLOC>
inline
auto
ConstAccessor<JaggedVecElt<PAYLOAD_T>, ALLOC>::getDataSpan (const AuxVectorData& container) const
  -> const_span
{
  const_Elt_span elt_span = getEltSpan (container);
  return const_span (elt_span,
                     ConstConverter_t (elt_span.data(),
                                       *container.getDataSpan (m_linkedAuxid)));
}


} // namespace SG
