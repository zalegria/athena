# G4FastSimulation
Author Vakho Tsulaia (tsulaia@mail.cern.ch)
Converted from packagedoc.h

## Introduction

This package provides some very basic interface to G4FastSimulations

## Class Overview

The classes included in this package are:

 - FastSimMenu : A python-available menu for dealing with fast simulations (and adding them to various volumes in the Geant4 simulation)
 - FastSimModelCatalog : A list of all available fast simulations
 - FastSimModel : A base class for fast simulations
 - FastSimModelProxy : A type for fast simulations to use for inclusion in the catalog
