# 
#  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#


##############################################################################
############################### SiT histograms ###############################
##############################################################################

###### Plane Occupancy ######

Input: [ 'AFP/SiT/HitsPerPlanes/planeHitsAllMU', 'AFP/SiT/Aux/eventsPerStation' ]
Output: [ 'AFP/SiT/PP/PlaneOccupancy/OccupancyAllMU' ]
Function: histgrinder.example.transform_function_divide_ROOT
Description: OccupancyAllMU0

---

### Synchronization ###

Input: [ 'AFP/SiT/Synchronization/Tracks/Front/Total_tracks_Front_profile_vs_lbTracksFront_(?P<station>[far|near]*[AC]side)', 'AFP/SiT/Synchronization/Tracks/End/Total_tracks_End_profile_vs_lbTracksEnd_(?P<station>[far|near]*[AC]side)']
Output: [ 'AFP/SiT/Synchronization/Tracks/RelativeDiff/Relative_difference_sync_tracks_{station}' ]
Function: Run3AFPMonitoring.AFPPostProcessing.relative_difference
Description: RelativeDiffSyncTracks

---

Input: [ 'AFP/SiT/Synchronization/Clusters/Front/clustersPerPlaneFrontPP_vs_lbClustersPerPlanesFront_(?P<station>[far|near]*[AC]side)_P(?P<plane>[0123])', 'AFP/SiT/Synchronization/Clusters/End/clustersPerPlaneEndPP_vs_lbClustersPerPlanesEnd_(?P<station>[far|near]*[AC]side)_P(?P<plane>[0123])']
Output: [ 'AFP/SiT/Synchronization/Clusters/RelativeDiff/Relative_difference_sync_clusters_{station}_P{plane}' ]
Function: Run3AFPMonitoring.AFPPostProcessing.relative_difference
Description: RelativeDiffSyncClusters

---

##############################################################################
############################### ToF histograms ###############################
##############################################################################

###### ToF hits divided by ToF events ######

Input: [ 'AFP/ToF/StationHits/ToFHits_side(?P<side>A|C)', 'AFP/ToF/Events/lb(?P<side>A|C)ToFEvents' ]
Output: [ 'AFP/ToF/PP/HitsByEvents/HitsByEvents_side{side}' ]
Function: histgrinder.example.transform_function_divide_ROOT
Description: HitsByEvents

---

###### ToFHitsVsLbByEvents ######

### Per bar ###

Input: [ 'AFP/ToF/ToFHitsVsLb/side(?P<side>A|C)/lb(?P=side)ToF_(?P<train>T[0123])_(?P<bar>[ABCD])', 'AFP/ToF/Events/lb(?P<side>A|C)ToFEvents' ]
Output: [ 'AFP/ToF/PP/ToFHitsVsLbByEvents/side{side}/lb{side}ToF_{train}_{bar}_Vs_Events' ]
Function: histgrinder.example.transform_function_divide_ROOT
Description: lbToF_train_bar_Vs_Events

---

### Per train (A+B+C+D) ###

# All
Input: [ 'AFP/ToF/ToFHitsVsLb/side(?P<side>A|C)/All/lb(?P=side)ToF_(?P<train>T[0123])', 'AFP/ToF/Events/lb(?P<side>A|C)ToFEvents' ]
Output: [ 'AFP/ToF/PP/ToFHitsVsLbByEvents/side{side}/All/lb{side}ToF_{train}_Vs_Events' ]
Function: histgrinder.example.transform_function_divide_ROOT
Description: lbToF_train_Vs_Events_All

---

# Front, Middle, End
Input: [ 'AFP/ToF/ToFHitsVsLb/side(?P<side>A|C)/(?P<fme>Front|Middle|End)/lb(?P=side)ToF_(?P<train>T[0123])_(?P=fme)', 'AFP/ToF/Events/lb(?P<side>A|C)ToFEvents' ]
Output: [ 'AFP/ToF/PP/ToFHitsVsLbByEvents/side{side}/{fme}/lb{side}ToF_{train}_Vs_Events_{fme}' ]
Function: histgrinder.example.transform_function_divide_ROOT
Description: lbAToF_train_Vs_Events_FME

---

##############################################################################
############################### ToF SiT histograms ###############################
##############################################################################

###### ToF efficiency ######

Input: [ 'AFP/ToFSiT/Efficiency/Efficiency_(?P<side>A|C)']
Output: [ 'AFP/ToFSiT/Efficiency/Efficiency_TH_{side}' ]
Function: Run3AFPMonitoring.AFPPostProcessing.tefficiency2th
Description: TEfficiency_to_TH2
