// Top level tool
#include "METUtilities/METMaker.h"
#include "METUtilities/METSystematicsTool.h"
#include "METUtilities/METSignificance.h"
// Algs
#include "../METMakerAlg.h"

using namespace met;

DECLARE_COMPONENT( METMaker )
DECLARE_COMPONENT( METSystematicsTool )
DECLARE_COMPONENT( METSignificance )

DECLARE_COMPONENT( METMakerAlg )
